$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
console.log("hello");
onPageLoading();

var gCombo = {
    kichCo: "",
    duongKinh: 0,
    suon: 0,
    salad: 0,
    soLuongNuoc: 0,
    thanhTien: 0
}

var gPizzaType = null;
var gMaDoUong = 0;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
 //on click to Button Gui don
 $("#form-gui-don").on("submit",function(event){
    event.preventDefault();
    onBtnGuiDon();
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
//loading Page at First
function onPageLoading(){
    callAjaxDrinkList();
} 
//change green button small click
    $("#btn-small").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-medium");
        ChangeToYellowColorButton("#btn-large");           
        console.log("changeColor")
        getComboPizza("S","20","2","200","2","150000");
        console.log(gCombo);
    })
    //change green button smedium click
    $("#btn-medium").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-small");
        ChangeToYellowColorButton("#btn-large");           
        console.log("changeColor")
        getComboPizza("M","25","4","300","3","200000");
        console.log(gCombo);
    })
    //change green button large click
    $("#btn-large").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-small");
        ChangeToYellowColorButton("#btn-medium");           
        console.log("changeColor")
        getComboPizza("L","30","28","500","4","250000");
        console.log(gCombo);
    })
    //change green button seafood click
    $("#btn-seafood").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-hawaii");
        ChangeToYellowColorButton("#btn-bacon");           
        console.log("changeColor")
        gPizzaType = "SEAFOOD"
    })
     //change green button hawaii click
     $("#btn-hawaii").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-seafood");
        ChangeToYellowColorButton("#btn-bacon");           
        console.log("changeColor")
        gPizzaType = "HAWAII";
    })

    //change green button bacon click
    $("#btn-bacon").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-seafood");
        ChangeToYellowColorButton("#btn-hawaii");           
        console.log("changeColor")
        gPizzaType = "BACON";
        console.log(gPizzaType);
    })


    //on change choose select Drinks
    $("#slt-do-uong").on("change",function(){
        gMaDoUong = $(this).val();
        console.log(gMaDoUong);
    })

    // On Click button Gui don 
    function onBtnGuiDon(){
        var vObjectRequest = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        }

        getNewOrder(vObjectRequest);
        if(!validate(vObjectRequest)){return;}
        console.log(vObjectRequest);
    }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
//FUNCTION change color button size Pizza


    //function CHange Green color Button
    function changeToGreenColorButton(paraButton){
        $(paraButton).attr("data-is-selected","Y");
        $(paraButton).removeClass("btn-warning");
        $(paraButton).addClass("btn-success");
    }
    //function CHange Yellow color Button
    function ChangeToYellowColorButton(paraButton){
        $(paraButton).attr("data-is-selected","N");
        $(paraButton).removeClass("btn-success");
        $(paraButton).addClass("btn-warning");
    }

    //mã nguồn để load data drink list (danh sách loại nước uống) về
    function onBtnGetDrinkListClick() {
        "use strict";
        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
        var vXhttp = new XMLHttpRequest();
        vXhttp.onreadystatechange =
            function () {
                if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                    console.log(vXhttp.responseText); //ghi response text ra console.log
                }
            };
            vXhttp.open("GET",vBASE_URL, true);
            vXhttp.send();
    }
    //call ajax drink list
    function callAjaxDrinkList(){
        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
        $.ajax({
            url: vBASE_URL,
            type: "GET",
            dataType: 'json',
            success: function(responseObject){
            loadDrinksToSelectElement(responseObject);
            console.log(responseObject);
            },
            error: function(error){
            console.assert(error.responseText);
            }
        });
    }
    //load List drink to select
    function loadDrinksToSelectElement(paraList){
        var vSelect = $("#slt-do-uong");
        for(var i =0 ; i< paraList.length;i++){
            var vOption = $("<option>",{
                value: paraList[i].maNuocUong,
                text: paraList[i].tenNuocUong
            }).appendTo(vSelect);
        }
    }

    //function get combo Pizza
    function getComboPizza(paraKichCo,paraDuongKinh,
        paraSuonNuong,paraSalad,paraNuocNgot,paraThanhTien){
            gCombo.kichCo = paraKichCo;
            gCombo.duongKinh = paraDuongKinh;
            gCombo.suonNuong = paraSalad;
            gCombo.salad = paraKichCo;
            gCombo.nuocNgot = paraNuocNgot;
            gCombo.thanhTien = paraThanhTien;
    }

    //create a new order
    function getNewOrder(paraObj){
        paraObj.kichCo = gCombo.kichCo;
        paraObj.duongKinh = gCombo.duongKinh;
        paraObj.suon = gCombo.suon;
        paraObj.salad = gCombo.salad;
        paraObj.soLuongNuoc = gCombo.soLuongNuoc;
        paraObj.thanhTien = gCombo.thanhTien;

        paraObj.loaiPizza = gPizzaType;

        paraObj.idLoaiNuocUong = gMaDoUong;

        paraObj.hoTen = $("#inp-name").val().trim();
        paraObj.email = $("#inp-email").val().trim();
        paraObj.soDienThoai = $("#inp-phone-number").val().trim();
        paraObj.diaChi = $("#inp-adrress").val().trim();
        paraObj.idVourcher = $("#inp-vouher-discount").val().trim();
        paraObj.loiNhan = $("#inp-messenger").val().trim();

    }

    //validate obj 
    function validate(paraObj){
        if(paraObj.kichCo == ""){
            alert("you didnt choose Size yet");
            return false;
        }

        if(paraObj.loaiPizza == null){
            alert("you didnt choose pizza type yet");
            return false;
        }

        if(paraObj.idLoaiNuocUong == 0){
            alert("you didnt choose drink type yet");
            return false;
        }

        if(paraObj.hoTen == "" || paraObj.soDienThoai == "" ||
        paraObj.diaChi == "" ){
            alert("invalide infomation");
            return false;
        }
        return true;


    }
})